//----------------------------------------------------------------------------------------------------------------------------------
//import the dependencies
const bcrypt = require("bcrypt");

const auth = require("../auth.js");
const Item = require("../models/item.js");

//----------------------------------------------------------------------------------------------------------------------------------
//add a product [admin only]
module.exports.addProduct = (body) => {
	let newItem = new Item({
		name: body.name,
		description: body.description,
		category: body.category,
		artist: body.artist,
		price: body.price,
		images: body.images,
		details: body.details,
		normalField: body.normalField,
		quantityStock: body.quantityStock
	});

	return newItem.save().then((item, err) => {
		if (err) {
			return {status: "error"};
		} else {
			return {status: "success"};
		};
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//get all products [admin only]
module.exports.getAllProducts = () => {
	return Item.find().then((item, err) => {
		if (err) {
			return {status: "error"};
		} else {
			return item;
		};
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//update specific product details [admin only]
module.exports.updateProduct = (params, body) => {
	let updatedProduct = {
		name: body.name,
		description: body.description,
		category: body.category,
		artist: body.artist,
		price: body.price,
		images: body.images,
		details: body.details,
		quantityStock: body.quantityStock
	};

	return Item.findByIdAndUpdate(params.productID, updatedProduct).then((item, err) => {
		if (item === null) {
			return {status: "notfound"};
		};

		if (err) {
			return {status: "error"};
		} else {
			return {status: "success"};
		};
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//archive specific product [admin only]
module.exports.archiveProduct = (params, body) => {
	let updatedProduct = {
		isActive: null
	};

	if (body.isActive === true) {
		updatedProduct.isActive = false;
	} else {
		updatedProduct.isActive = true;
	}

	return Item.findByIdAndUpdate(params.productID, updatedProduct).then((item, err) => {
		if (item === null) {
			return "This product does not exist."
		};

		if (err) {
			return {status: "error"};
		} else {
			return {status: "success"};
		};
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//delete specific product [admin only]
module.exports.deleteProduct = (params) => {
	return Item.findByIdAndDelete(params.productID).then((item, err) => {
		if (err) {
			return {status: "error"};
		} else {
			return {status: "success"};
		};
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//get all active products
module.exports.getActiveProducts = () => {
	return Item.find({isActive: true}).then((item, err) => {
		if (err) {
			return {status: "error"};
		} else {
			return item;
		};
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//get a specific product
module.exports.getOneProduct = (params) => {
	return Item.find({_id: params.productID}).then((item, err) => {
		if (item.length === 0) {
			return "This product does not exist."
		};

		if (err) {
			return {status: "error"};
		} else {
			return item;
		};
	});
};

//----------------------------------------------------------------------------------------------------------------------------------