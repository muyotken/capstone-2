//----------------------------------------------------------------------------------------------------------------------------------
//import the dependencies
const bcrypt = require("bcrypt");

const auth = require("../auth.js");
const User = require("../models/user.js");

//----------------------------------------------------------------------------------------------------------------------------------
//register the user
module.exports.registerUser = (body) => {
	return User.find({emailAddress: body.emailAddress}).then(out => {
		if (out.length > 0) {
			return {status: "taken"};
		} else {
			let newUser = new User({
				emailAddress: body.emailAddress,
				password: bcrypt.hashSync(body.password, 10),
				fullName: body.fullName,
				contactNumber: body.contactNumber,
				birthDate: body.birthDate
			});

			return newUser.save().then((user, err) => {
				if (err) {
					return {status: "error"};
				} else {
					return {status: "success"};
				};
			});
		};
	});	
};

//----------------------------------------------------------------------------------------------------------------------------------
//login functionality
module.exports.loginUser = (body) => {
	return User.findOne({emailAddress: body.emailAddress}).then(out => {
		if (out === null) {
			return {status: "notfound"};
		} else {
			const isPasswordCorrect = bcrypt.compareSync(body.password, out.password);

			if (isPasswordCorrect) {
				return {access: auth.createToken(out.toObject())};
			} else {
				return {status: "passwordincorrect"};
			};
		};
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//get the profile of the logged-in user
module.exports.getProfile = (userID) => {
	return User.findById(userID).then(out => {
		out.password = undefined;
		
		let getProfileOut = {
			userID: out._id,
			isAdmin: out.isAdmin,
			emailAddress: out.emailAddress,
			fullName: out.fullName,
			profilePicture: out.profilePicture,
			contactNumber: out.contactNumber,
			birthDate: out.birthDate,
			dateRegistered: out.dateRegistered
		};

		return getProfileOut;
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//update the profile of the logged-in user
module.exports.updateProfile = (userID, body) => {
	let updatedProfile = {
		fullName: body.fullName,
		profilePicture: body.profilePicture,
		contactNumber: body.contactNumber
	};

	return User.findByIdAndUpdate(userID, updatedProfile).then((user, err) => {
		if (err) {
			return {status: "error"};
		} else {
			return {status: "success"};
		}
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//get the addresses of the logged-in user
module.exports.getAddresses = (userID) => {
	return User.findById(userID).then(out => {

		return out.shippingAddresses;
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//get a specific address of the logged-in user
module.exports.getSpecificAddress = (userID, params) => {
	return User.findById(userID).then(out => {
		for (i = 0; i < out.shippingAddresses.length; i++) {
			if (out.shippingAddresses[i]._id == params.addressID) {
				return out.shippingAddresses[i];
			};
		};
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//add an address for the logged-in user
module.exports.addAddress = (userID, body) => {
	return User.findById(userID).then(out => {
		out.shippingAddresses.push({
			rcptLabel: body.rcptLabel,
			rcptName: body.rcptName,
			rcptContactNumber: body.rcptContactNumber,
			rcptUnit: body.rcptUnit,
			rcptStreet: body.rcptStreet,
			rcptBarangay: body.rcptBarangay,
			rcptCity: body.rcptCity,
			rcptProvince: body.rcptProvince,
			rcptZip: body.rcptZip,
			rcptNotes: body.rcptNotes
		});

		return out.save().then((user, err) => {
			if (err) {
				return {status: "error"};
			} else {
				return {status: "success"};
			};
		});
	});	
};

//----------------------------------------------------------------------------------------------------------------------------------
//update a specific address of the logged-in user
module.exports.updateAddress = (userID, params, body) => {
	return User.findById(userID).then(out => {
		for (i = 0; i < out.shippingAddresses.length; i++) {
			if (out.shippingAddresses[i]._id == params.addressID) {
				out.shippingAddresses[i].rcptLabel = body.rcptLabel
				out.shippingAddresses[i].rcptName = body.rcptName
				out.shippingAddresses[i].rcptContactNumber = body.rcptContactNumber
				out.shippingAddresses[i].rcptUnit = body.rcptUnit
				out.shippingAddresses[i].rcptStreet = body.rcptStreet
				out.shippingAddresses[i].rcptBarangay = body.rcptBarangay
				out.shippingAddresses[i].rcptCity = body.rcptCity
				out.shippingAddresses[i].rcptProvince = body.rcptProvince
				out.shippingAddresses[i].rcptZip = body.rcptZip
				out.shippingAddresses[i].rcptNotes = body.rcptNotes

				return out.save().then((user, err) => {
					if (err) {
						return {status: "error"};
					} else {
						return {status: "success"};
					};
				});
			};
		};
	});	
};

//----------------------------------------------------------------------------------------------------------------------------------
//delete a specific address of the logged-in user
module.exports.deleteAddress = (userID, params) => {
	return User.findById(userID).then(out => {
		for (i = 0; i < out.shippingAddresses.length; i++) {
			if (out.shippingAddresses[i]._id == params.addressID) {
				out.shippingAddresses.splice(i, 1);

				return out.save().then((user, err) => {
					if (err) {
						return {status: "error"};
					} else {
						return {status: "success"};
					};
				});
			};
		};
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//get all users [admin only]
module.exports.getAllUsers = () => {
	return User.find({},{
		_id: 1,
		fullName: 1,
		emailAddress: 1,
		contactNumber: 1,
		dateRegistered: 1,
		isAdmin: 1
	}).then((user, err) => {
		if (err) {
			return {status: "error"};
		} else {
			return user;
		};
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//archive specific product [admin only]
module.exports.makeAdmin = (body) => {
	let updatedUser = {
		isAdmin: null
	};

	return User.findById(body.userID).then(out => {
		if (out === null) {
			return "This user does not exist."
		} else {
			if (body.isAdmin === false) {
				updatedUser.isAdmin = true
			} else {
				updatedUser.isAdmin = false
			};

			return User.findByIdAndUpdate(body.userID, updatedUser).then((user, err) => {
				if (err) {
					return {status: "error"};
				} else {
					return {status: "success"};
				};
			});
		};
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//change password functionality
module.exports.changePassword = (userID, body) => {
	return User.findById(userID).then(out => {
		const isPasswordCorrect = bcrypt.compareSync(body.oldPassword, out.password);

		if (isPasswordCorrect) {
			out.password = bcrypt.hashSync(body.newPassword, 10);

			return out.save().then((user, err) => {
				if (err) {
					return {status: "error"};
				} else {
					return {status: "success"};
				};
			});
		} else {
			return {status: "oldincorrect"};
		};
	});
};

//----------------------------------------------------------------------------------------------------------------------------------