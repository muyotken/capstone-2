//----------------------------------------------------------------------------------------------------------------------------------
const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	buyerEmailAddress: {
		type: String,
		required: [true, "Buyer's email address is required."]
	},
	datePlaced: {
		type: Date,
		default: new Date()
	},
	/*shippingAddress: {
		rcptName: {
			type: String,
			required: [true, "Recipient name is required."]
		},
		rcptContactNumber: {
			type: String,
			required: [true, "Recipient contact number is required."]
		},
		rcptLabel: {
			type: String,
			enum: ["Office", "Residence"],
			required: [true, "Address label is required."]
		},
		rcptUnit: {
			type: String,
			required: [true, "House/Unit/Flr #, Bldg Name, Blk, Lot # is required."]
		},
		rcptStreet: {
			type: String,
			required: [true, "Street address is required."]
		},
		rcptBarangay: {
			type: String,
			required: [true, "Barangay is required."]
		},
		rcptCity: {
			type: String,
			required: [true, "City/Municipality is required."]
		},
		rcptProvince: {
			type: String,
			required: [true, "Province is required."]
		},
		rcptNotes: {
			type: String
		}
	},*/
	itemsOrdered: [
		{
			itemID: {
				type: String,
				required: [true, "Add atleast one product for checkout. 1"]
			},
			itemName: {
				type: String,
				required: [true, "Add atleast one product for checkout. 2"]
			},
			quantityOrdered: {
				type: Number,
				required: [true, "Add atleast one product for checkout. 2"]
			},
			itemSubTotal: {
				type: Number,
				required: [true, "Add atleast one product for checkout. 3"]
			},
			itemArtist: {
				type: String,
				required: [true, "Item artist is required."]
			},
			itemPrice: {
				type: Number,
				required: [true, "Price is required."]
			},
			itemCategory: {
				type: String,
				enum: ["Albums", "Photocards", "Lightsticks", "Other Merch"],
				required: [true, "Item category is required."]
			},
			itemImage: {
				type: String
			}
		}
	],
	orderAmount: {
		type: Number,
		required: [true, "Order amount is required."]
	},
	orderStatus: {
		type: String,
		enum: ["To Ship", "Shipped", "Completed"],
		default: "To Ship"
	}
});

module.exports = mongoose.model("Order", orderSchema);
//----------------------------------------------------------------------------------------------------------------------------------