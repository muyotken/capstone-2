//----------------------------------------------------------------------------------------------------------------------------------
const jwt = require("jsonwebtoken");
const secret = "PopKollabECommerceAPI";

//----------------------------------------------------------------------------------------------------------------------------------
//Creation of the token
module.exports.createToken = (user) => {
	const tokenData = {
		tokenUserID: user._id,
		tokenEmail: user.emailAddress,
		tokenIsAdmin: user.isAdmin
	};

	return jwt.sign(tokenData, secret, {});
};

//----------------------------------------------------------------------------------------------------------------------------------
//Verification of the token
module.exports.verifyToken = (req, res, next) => {
	let token = req.headers.authorization;

	if (typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return res.send({auth: "failed"});
			} else {
				next();
			};
		});
	} else {
		return res.send({auth: "User is not logged in."});
	};
};

//----------------------------------------------------------------------------------------------------------------------------------
//Decode the token
module.exports.decodeToken = (token) => {
	if (typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return null;
			} else {
				return jwt.decode(token, {complete: true}).payload;
			};
		});
	} else {
		return null;
	};
};

//----------------------------------------------------------------------------------------------------------------------------------