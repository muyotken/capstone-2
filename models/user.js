//----------------------------------------------------------------------------------------------------------------------------------
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	emailAddress: {
		type: String,
		required: [true, "Email address is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	fullName: {
		type: String,
		required: [true, "Name is required."]
	},
	profilePicture: {
		data: Buffer,
		contentType: String
	},
	shippingAddresses: [
		{
			rcptLabel: {
				type: String,
				enum: ["Office", "Home"],
				default: "Home",
				required: [true, "Address label is required."]
			},
			rcptName: {
				type: String,
				required: [true, "Recipient name is required."]
			},
			rcptContactNumber: {
				type: String,
				required: [true, "Recipient contact number is required."]
			},
			rcptUnit: {
				type: String,
				required: [true, "House/Unit/Flr #, Bldg Name, Blk, Lot # is required."]
			},
			rcptStreet: {
				type: String,
				required: [true, "Street address is required."]
			},
			rcptBarangay: {
				type: String,
				required: [true, "Barangay is required."]
			},
			rcptCity: {
				type: String,
				required: [true, "City/Municipality is required."]
			},
			rcptProvince: {
				type: String,
				required: [true, "Province is required."]
			},
			rcptZip: {
				type: String,
				required: [true, "ZIP is required."]
			},
			rcptNotes: {
				type: String
			}
		}
	],
	contactNumber: {
		type: String,
		required: [true, "Contact number is required."]
	},
	birthDate: {
		type: Date
	},
	dateRegistered: {
		type: Date,
		default: new Date()
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	cartItems: [
		{
			productID: String
		}
	],
	wishListItems: [
		{
			productID: String
		}
	]
});

module.exports = mongoose.model("User", userSchema);
//----------------------------------------------------------------------------------------------------------------------------------