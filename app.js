//----------------------------------------------------------------------------------------------------------------------------------
//set up the dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require('body-parser');

//----------------------------------------------------------------------------------------------------------------------------------
//application routes
const userRoutes = require("./routes/userRoutes.js");
const itemRoutes = require("./routes/itemRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js");

//----------------------------------------------------------------------------------------------------------------------------------
//set up the server
const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json({limit:'100mb'}));
app.use(bodyParser.urlencoded({ limit:'100mb', extended: true }));

const corsOptions = {
	origin: ['http://localhost:3000','https://pensive-liskov-dfffba.netlify.app'],
	optionsSuccessStatus: 200
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({extended: true}));


//----------------------------------------------------------------------------------------------------------------------------------
//connect to the database
mongoose.connect("mongodb+srv://admin:admin123@cluster0.xzo32.mongodb.net/capstone-2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas."));

//----------------------------------------------------------------------------------------------------------------------------------
//application routes
app.use("/users", userRoutes);
app.use("/items", itemRoutes);
app.use("/orders", orderRoutes);

//----------------------------------------------------------------------------------------------------------------------------------
app.listen(port, () => console.log(`Server running at port ${port}.`));