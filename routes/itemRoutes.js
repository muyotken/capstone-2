//----------------------------------------------------------------------------------------------------------------------------------
//import the dependencies
const express = require("express");
const router = express.Router();

const auth = require("../auth.js");
const itemController = require("../controllers/itemController.js");

//----------------------------------------------------------------------------------------------------------------------------------
//route for adding a new product [admin only]
router.post("/add", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	if (userData.tokenIsAdmin) {
		itemController.addProduct(req.body).then(addProdRes => res.send(addProdRes));
	} else {
		res.send({auth: "Not an admin account."});
	};
});

//----------------------------------------------------------------------------------------------------------------------------------
//route for getting all products in database [admin only]
router.get("/all", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	if (userData.tokenIsAdmin) {
		itemController.getAllProducts().then(getAllProdRes => res.send(getAllProdRes));
	} else {
		res.send({auth: "Not an admin account."});
	};
});

//----------------------------------------------------------------------------------------------------------------------------------
//route for updating a specific product [admin only]
router.put("/:productID", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	if (userData.tokenIsAdmin) {
		itemController.updateProduct(req.params, req.body).then(updProdRes => res.send(updProdRes));
	} else {
		res.send({auth: "Not an admin account."});
	};
});

//----------------------------------------------------------------------------------------------------------------------------------
//route for archiving a product [admin only]
router.delete("/:productID", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	if (userData.tokenIsAdmin) {
		itemController.archiveProduct(req.params, req.body).then(archProdRes => res.send(archProdRes));
	} else {
		res.send({auth: "Not an admin account."});
	};
});

//----------------------------------------------------------------------------------------------------------------------------------
//route for archiving a product [admin only]
router.delete("/delete/:productID", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	if (userData.tokenIsAdmin) {
		itemController.deleteProduct(req.params).then(delProdRes => res.send(delProdRes));
	} else {
		res.send({auth: "Not an admin account."});
	};
});

//----------------------------------------------------------------------------------------------------------------------------------
//route for getting all active products in the database
router.get("/active", (req, res) => {
	itemController.getActiveProducts().then(getActProdRes => res.send(getActProdRes));
});

//----------------------------------------------------------------------------------------------------------------------------------
//route for getting a specific product in the database
router.get("/:productID", (req, res) => {
	itemController.getOneProduct(req.params).then(getOneProdRes => res.send(getOneProdRes));
});

module.exports = router;
//----------------------------------------------------------------------------------------------------------------------------------