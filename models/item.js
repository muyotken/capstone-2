//----------------------------------------------------------------------------------------------------------------------------------
const mongoose = require("mongoose");

const itemSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Item name is required."]
	},
	description: {
		type: String,
		required: [true, "Item description is required."]
	},
	category: {
		type: String,
		enum: ["Albums", "Photocards", "Lightsticks", "Other Merch"],
		required: [true, "Item category is required."]
	},
	artist: {
		type: String,
		required: [true, "Item artist is required."]
	},
	ratingDetails: {
		ratingScore: {
			type: Number,
			min: 0,
			max: 5,
			default: 0
		},
		numberOfRatings: {
			type: Number,
			min: 0,
			default: 0
		}
	},
	quantitySold: {
		type: Number,
		min: 0,
		default: 0
	},
	price: {
		type: Number,
		required: [true, "Price is required."]
	},
	images: [
		{
			type: String
		}
	],
	details: {
		type: String
	},
	reviewDetails: [
		{
			rvwrUser: {
				type: String,
				required: [true, "Reviewer's name is required."]
			},
			rvwrScore: {
				type: Number,
				min: 0,
				max: 5,
				required: [true, "Reviewer's score is required."]
			},
			rvwrBody: {
				type: String
			},
			rvwrImages: [
				{
					data: Buffer,
					contentType: String
				}
			]
		}
	],
	quantityStock: {
		type: Number,
		min: 0,
		required: [true, "Item stock quantity is required."],
		default: 0
	},
	isActive: {
		type: Boolean,
		default: true
	}
});

module.exports = mongoose.model("Item", itemSchema);
//----------------------------------------------------------------------------------------------------------------------------------