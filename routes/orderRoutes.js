//----------------------------------------------------------------------------------------------------------------------------------
//import the dependencies
const express = require("express");
const router = express.Router();

const auth = require("../auth.js");
const orderController = require("../controllers/orderController.js");

//----------------------------------------------------------------------------------------------------------------------------------
//route for creating a new order record
router.post("/checkout", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	orderController.createOrder(userData.tokenEmail, req.body).then(createOrderRes => res.send(createOrderRes));
});

//----------------------------------------------------------------------------------------------------------------------------------
//route for getting all orders placed by the logged-in user
router.get("/", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	orderController.getOrders(userData.tokenEmail).then(getOrdersRes => res.send(getOrdersRes));
});

//----------------------------------------------------------------------------------------------------------------------------------
//route for retrieving all orders placed [admin only]
router.get("/all", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	if (userData.tokenIsAdmin) {
		orderController.getAllOrders().then(getAllOrdersRes => res.send(getAllOrdersRes));
	} else {
		res.send({auth: "Not an admin account."});
	};
});

//----------------------------------------------------------------------------------------------------------------------------------
//route to add a review
router.post("/:itemID", auth.verifyToken, (req, res) => {
	orderController.addReview(req.params, req.body).then(addReviewRes => res.send(addReviewRes));
});

module.exports = router;
//----------------------------------------------------------------------------------------------------------------------------------