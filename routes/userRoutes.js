//----------------------------------------------------------------------------------------------------------------------------------
//import the dependencies
const express = require("express");
const router = express.Router();

const auth = require("../auth.js");
const userController = require("../controllers/userController.js");

//----------------------------------------------------------------------------------------------------------------------------------
//route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(registerRes => res.send(registerRes));
});

//route for logging in
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(LoginRes => res.send(LoginRes));
});

//----------------------------------------------------------------------------------------------------------------------------------
//route for getting user profile
router.get("/profile", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	userController.getProfile(userData.tokenUserID).then(getProfileRes => res.send(getProfileRes));
});

//route for updating user profile
router.put("/profile", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	userController.updateProfile(userData.tokenUserID, req.body).then(updProfileRes => res.send(updProfileRes));
});

//----------------------------------------------------------------------------------------------------------------------------------
//route for getting all addresses
router.get("/addresses", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	userController.getAddresses(userData.tokenUserID).then(getAddRes => res.send(getAddRes));
});

//route for getting a specific address
router.get("/addresses/:addressID", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	userController.getSpecificAddress(userData.tokenUserID, req.params).then(getSpecificRes => res.send(getSpecificRes));
});

//route for adding a new address
router.post("/addresses", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	userController.addAddress(userData.tokenUserID, req.body).then(addAddRes => res.send(addAddRes));
});

//route for updating a specific address
router.put("/addresses/:addressID", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	userController.updateAddress(userData.tokenUserID, req.params, req.body).then(updAddRes => res.send(updAddRes));
});

//route for deleting a specific address
router.delete("/addresses/:addressID", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	userController.deleteAddress(userData.tokenUserID, req.params).then(delAddRes => res.send(delAddRes));
});

//----------------------------------------------------------------------------------------------------------------------------------
//route for getting all users in the database [admin only]
router.get("/all", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	if (userData.tokenIsAdmin) {
		userController.getAllUsers().then(getAllUsersRes => res.send(getAllUsersRes));
	} else {
		res.send({auth: "Not an admin account."});
	};
});

//----------------------------------------------------------------------------------------------------------------------------------
//route for setting a non-admin user to an admin user [admin only]
router.put("/makeAdmin", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	if (userData.tokenIsAdmin) {
		userController.makeAdmin(req.body).then(makeAdminRes => res.send(makeAdminRes));
	} else {
		res.send({auth: "Not an admin account."});
	};
});

//----------------------------------------------------------------------------------------------------------------------------------
//route for changing the password
router.put("/changePassword", auth.verifyToken, (req, res) => {
	const userData = auth.decodeToken(req.headers.authorization);

	userController.changePassword(userData.tokenUserID, req.body).then(changePassRes => res.send(changePassRes));
});

module.exports = router;
//----------------------------------------------------------------------------------------------------------------------------------