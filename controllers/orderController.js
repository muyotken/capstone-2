//----------------------------------------------------------------------------------------------------------------------------------
//import the dependencies
const bcrypt = require("bcrypt");

const auth = require("../auth.js");
const Order = require("../models/order.js");
const Item = require("../models/item.js");

//----------------------------------------------------------------------------------------------------------------------------------
//create an order
module.exports.createOrder = async (tokenEmail, body) => {
	if (body.itemsOrdered.length === 0) {
		return "Add atleast one item for checkout."
	};

	let newOrder = new Order({
		buyerEmailAddress: tokenEmail,
		/*shippingAddress: {
			rcptName: body.shippingAddress.rcptName,
			rcptContactNumber: body.shippingAddress.rcptContactNumber,
			rcptLabel: body.shippingAddress.rcptLabel,
			rcptUnit: body.shippingAddress.rcptUnit,
			rcptStreet: body.shippingAddress.rcptStreet,
			rcptBarangay: body.shippingAddress.rcptBarangay,
			rcptCity: body.shippingAddress.rcptCity,
			rcptProvince: body.shippingAddress.rcptProvince,
			rcptNotes: body.shippingAddress.rcptNotes
		},*/
		orderAmount: body.orderAmount
	});

	for (i = 0; i < body.itemsOrdered.length; i++) {
		newOrder.itemsOrdered.push({
				itemID: body.itemsOrdered[i].itemID,
				itemName: body.itemsOrdered[i].itemName,
				itemArtist: body.itemsOrdered[i].itemArtist,
				itemPrice: body.itemsOrdered[i].itemPrice,
				itemCategory: body.itemsOrdered[i].itemCategory,
				itemImage: body.itemsOrdered[i].itemImage,
				quantityOrdered: body.itemsOrdered[i].quantityOrdered,
				itemSubTotal: body.itemsOrdered[i].itemSubTotal
		});

		await Item.findByIdAndUpdate(
			body.itemsOrdered[i].itemID,
			{
				$inc: {
					quantityStock: -body.itemsOrdered[i].quantityOrdered, 
					quantitySold: body.itemsOrdered[i].quantityOrdered
				}
			}
		).then((item, err) => {
			if (err) {
				return "Error in updating quantity";
			};
		});
	};

	return newOrder.save().then((order, err) => {
		if (err) {
			return {status: "error"};
		} else {
			return {status: "success"};
		};
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//get all orders for the logged-in user
module.exports.getOrders = (userEmail) => {
	return Order.find({buyerEmailAddress: userEmail}).then((order, err) => {
		if (err) {
			return {status: "error"};
		} else {
			return order;
		};
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//get all orders placed [admin only]
module.exports.getAllOrders = () => {
	return Order.find().then((order, err) => {
		if (err) {
			return {status: "error"};
		} else {
			return order;
		};
	});
};

//----------------------------------------------------------------------------------------------------------------------------------
//add a review
module.exports.addReview = (params, body) => {
	return Item.findById(params.itemID).then(out => {
		if (out === null) {
			return "This product does not exist.";
		};

		out.reviewDetails.push({
			rvwrUser: body.rvwrUser,
			rvwrScore: body.rvwrScore,
			rvwrBody: body.rvwrBody,
			rvwrImages: body.rvwrImages
		});

		out.ratingDetails.numberOfRatings += 1;

		let newRatingScore = 0;

		//accumulate all scores from all reviews and get average
		for (i = 0; i < out.reviewDetails.length; i++) {
			newRatingScore += out.reviewDetails[i].rvwrScore;
		}

		out.ratingDetails.ratingScore = newRatingScore / out.ratingDetails.numberOfRatings;

		return out.save().then((item, err) => {
			if (err) {
				return {status: "error"};
			} else {
				return {status: "success"};
			};
		});
	});
};

//----------------------------------------------------------------------------------------------------------------------------------